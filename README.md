## Teste Melhor Envio

### Considerações

Minha ideia principal seria criar uma rotina que de tempo em tempo chamasse registrando os dados no banco, porém não consegui finalizar a rotina.

### Instação

Rodar o composer 
``` sh
composer install
```

Iniciar o sail (docker)
``` sh
./vendor/bin/sail up -d
```

Rodar as migrations
``` sh
./vendor/bin/sail php artisan migrate
```

Colocar o arquivo logs.txt na pasta 
``` sh
storage/app/logs/logs.txt
```

``` sh
yarn install
```

``` sh
yarn run prod
```

### Para somente iniciar o projeto
``` sh
./vendor/bin/sail up -d
```

Acessar a página em
``` sh
http://localhost/requests
```

### Para rodar o processo de salvar os requests
Rodar o comando
``` sh
./vendor/bin/sail php artisan process:requests
```
