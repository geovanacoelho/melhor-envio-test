<table>
    <thead>
    <tr>
        <th>Service ID</th>
        <th>Média Request</th>
        <th>Média Proxy</th>
        <th>Média Kong</th>
    </tr>
    </thead>
    <tbody>
        @foreach($requests as $request)
            <tr>
                <td>{{ $request->service_id }}</td>
                <td>{{ $request->request_media }}</td>
                <td>{{ $request->proxy_media }}</td>
                <td>{{ $request->kong_media }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
