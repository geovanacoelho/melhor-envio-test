<table>
    <thead>
    <tr>
        <th>Customer ID</th>
        <th>Request</th>
        <th>Proxy</th>
        <th>Kong</th>
    </tr>
    </thead>
    <tbody>
    @foreach($requests as $request)
        <tr>
            <td>{{ $request->customer_id }}</td>
            <td>{{ $request->request }}</td>
            <td>{{ $request->proxy }}</td>
            <td>{{ $request->kong }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
