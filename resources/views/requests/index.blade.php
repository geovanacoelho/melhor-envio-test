@extends('layouts.app')

@section('content')

    @if($errors->all())
        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative mb-3" role="alert">
            @foreach ($errors->all() as $error)
                <span class="block">{{ $error }}</span>
            @endforeach
        </div>
    @endif

    <div class="flex flex-col">
        <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">

                <form method="GET" action="{{route('download')}}" class="shadow overflow-hidden border-b bg-gray-50 sm:rounded-lg mb-5 flex items-end p-4">
                    <div class="flex-1 px-2">
                        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="service">
                            Service
                        </label>
                        <div class="relative">
                            <select
                                class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                name="service_id" id="service">
                                @foreach($services as $service)
                                    <option value="{{$service->id}}">{{$service->id}}</option>
                                @endforeach
                            </select>
                            <div
                                class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg"
                                     viewBox="0 0 20 20">
                                    <path
                                        d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                                </svg>
                            </div>
                        </div>
                    </div>

                    <div class="flex-1 px-2">
                        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="service">
                            Customer
                        </label>
                        <div class="relative">
                        <select  class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                 name="customer_id" id="customer">
                            @foreach($customers as $customer)
                                <option value="{{$customer->id}}">{{$customer->id}}</option>
                            @endforeach
                        </select>
                        <div
                            class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                            <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg"
                                 viewBox="0 0 20 20">
                                <path
                                    d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                            </svg>
                        </div>
                        </div>
                    </div>

                    <div>
                        <div>
                            <input type="radio" id="service" name="download_type" value="service"
                                   checked>
                            <label for="service">Service</label>
                        </div>

                        <div>
                            <input type="radio" id="customer" name="download_type" value="customer">
                            <label for="customer">Customers</label>
                        </div>

                        <div>
                            <input type="radio" id="media" name="download_type" value="media">
                            <label for="media">Tempo médio</label>
                        </div>

                        <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                            Baixar
                        </button>
                    </div>
                </form>
                <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                    <table class="min-w-full divide-y divide-gray-200">
                        <thead class="bg-gray-50">
                        <tr>
                            @foreach($labels as $label)
                                <th scope="col"
                                    class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    {{$label}}
                                </th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody class="bg-white divide-y divide-gray-200">
                        @if(count($data))
                            @foreach ($data as $key => $value)
                                <tr>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="text-sm text-gray-500">{{ $value->customer_id }}</div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="text-sm text-gray-500">{{ $value->customer_id }}</div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="text-sm text-gray-500">{{ $value->request }}</div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="text-sm text-gray-500">{{ $value->proxy }}</div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="text-sm text-gray-500">{{ $value->kong }}</div>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5">
                                    <p class="text-center py-2">Ainda não possui nenhum registro</p>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @if ($data->lastPage() > 1)
        <div class="text-center my-4">
            <nav class="relative z-0 inline-flex rounded-md shadow-sm -space-x-px" aria-label="Pagination">
                <a href="{{ $data->url(1) }}"
                   class="relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50">
                    <span class="sr-only">Previous</span>
                    <svg class="h-5 w-5" x-description="Heroicon name: solid/chevron-left"
                         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                        <path fill-rule="evenodd"
                              d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                              clip-rule="evenodd"></path>
                    </svg>
                </a>
                <!-- Current: "z-10 bg-indigo-50 border-indigo-500 text-indigo-600", Default: "bg-white border-gray-300 text-gray-500 hover:bg-gray-50" -->
                @for ($i = 1; $i <= $data->lastPage(); $i++)
                    <a href="{{ $data->url($i) }}" aria-current="page"
                       class="{{
                        (($data->currentPage() == $i) ?
                        'z-10 bg-indigo-50 border-indigo-500 text-indigo-600':
                         'bg-white border-gray-300 text-gray-500 hover:bg-gray-50v') . 'relative inline-flex items-center px-4 py-2 border text-sm font-medium'
                         }}">
                        {{ $i }}
                    </a>
                @endfor
                <a href="{{ $data->url($data->currentPage()+1) }}"
                   class="relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50">
                    <span class="sr-only">Next</span>
                    <svg class="h-5 w-5" x-description="Heroicon name: solid/chevron-right"
                         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                        <path fill-rule="evenodd"
                              d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                              clip-rule="evenodd"></path>
                    </svg>
                </a>
            </nav>
        </div>
    @endif
@endsection

