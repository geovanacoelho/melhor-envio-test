<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequestLogs extends Model
{
    use HasFactory;

    public $incrementing = false;

    protected $fillable = ['id', 'request'];

    protected $casts = [
        'request' => 'json',
    ];

    public function setDataAttribute($value): void
    {
        if (is_array($value)) {
            $this->attributes['request'] = json_encode($value);
        }
    }
}
