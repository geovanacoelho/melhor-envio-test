<?php

namespace App\Exports;

use App\Models\Request;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class CustomerExport implements FromView
{
    public function __construct(string $customer_id)
    {
        $this->customer_id = $customer_id;
    }

    public function view(): View
    {
        return view('exports.customers', [
            'requests' => Request::where('customer_id', $this->customer_id)->with('customer')->get()
        ]);
    }
}
