<?php

namespace App\Exports;

use App\Models\Request;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ServiceExport implements FromView
{
    public function __construct(string $service_id)
    {
        $this->service_id = $service_id;
    }

    public function view(): View
    {
        return view('exports.services', [
            'requests' => Request::where('service_id', $this->service_id)->with('service', 'customer')->get()
        ]);
    }
}
