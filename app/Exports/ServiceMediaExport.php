<?php

namespace App\Exports;

use App\Models\Request as ModelRequest;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromView;

class ServiceMediaExport implements FromView
{
    public function view(): View
    {

        $requests = ModelRequest::select([
            'service_id',
            DB::raw('round(AVG(request),0) as request_media'),
            DB::raw('round(AVG(proxy),0) as proxy_media'),
            DB::raw('round(AVG(kong),0) as kong_media')
        ])
            ->groupBy('service_id')
            ->get();

        return view('exports.services-media', [
            'requests' => $requests
        ]);
    }
}
