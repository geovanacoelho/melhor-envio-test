<?php

namespace App\Http\Controllers;

use App\Exports\CustomerExport;
use App\Exports\ServiceExport;
use App\Exports\ServiceMediaExport;
use App\Models\Customer as ModelCustomer;
use App\Models\Request as ModelRequest;
use App\Models\Service as ModelService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class RequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $data = ModelRequest::latest()->paginate(10);

        $customers = ModelCustomer::select('id')->get();
        $services  = ModelService::select('id')->get();

        return view('requests.index', compact('data'))
            ->with([
                'labels' => ['Customer ID', 'Service ID', 'Request', 'Proxy', 'Kong'],
                'customers' => $customers,
                'services' => $services
            ]);
    }

    public function download(Request $request)
    {
        $request->validate([
            'download_type' => 'in:service,customer,media',
            'service_id' => ($request->service_id) ? 'required_if:download_type,in:service|exists:services,id' : 'required',
            'customer_id' => ($request->customer_id) ? 'required_if:download_type,in:customer|exists:customers,id' : 'required'
        ]);

        if ($request->download_type === 'service') {
            return Excel::download(new ServiceExport($request->service_id), 'services.csv');
        }

        if ($request->download_type === 'customer') {
            return Excel::download(new CustomerExport($request->customer_id), 'customers.csv');
        }

        if ($request->download_type === 'media') {

            return Excel::download(new ServiceMediaExport(), 'services-media.csv');

        }
    }
}
