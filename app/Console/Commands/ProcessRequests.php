<?php

namespace App\Console\Commands;

use App\Models\Customer;
use App\Models\Request;
use App\Models\RequestLogs;
use App\Models\Service;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ProcessRequests extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:requests';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process requests and save to database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = 'logs.txt';

        if (Storage::disk('local')->exists("logs/{$name}")) {
            $path = Storage::path("logs/$name");

            $lines_of_file = file($path);

            $hasProccess = $this->hasProccess($lines_of_file);

            if ($hasProccess === false) {
                return 0;
            }

            [$offset, $length] = $hasProccess;

            $first3 = array_slice($lines_of_file, $offset, $length);

            foreach ($first3 as $line) {
                $dataGetry = json_decode($line);

                $authenticated_entity = $dataGetry->authenticated_entity;
                $service              = $dataGetry->service;

                $_Customer = $this->createOrGetCustomer($authenticated_entity->consumer_id->uuid);

                $_Service    = $this->createOrGetService($service->id, $service);
                $_RequestLog = $this->createOrGetRequestLog($dataGetry);


                $this->saveRequest($_Customer->id, $_Service->id, $_RequestLog->id, $dataGetry->latencies);
            }
        }

        return 0;
    }

    public function hasProccess($lines, $offset = 0, $countLine = 10)
    {
        $countAdd = 10;
        $maxLines = count($lines);

        if ($offset >= $maxLines) {
            return false;
        }

        $first3        = array_slice($lines, $offset, $countLine);
        $linesProccess = [];

        foreach ($first3 as $line) {
            $linesProccess[] = json_encode(json_decode($line));
        }

        $requestLog = RequestLogs::whereIn('request', $linesProccess)->get();

        if (count($requestLog) === count($linesProccess)) {
            return $this->hasProccess($lines, $offset + $countAdd, $countLine + $countAdd);
        }

        return [$offset, $countLine];
    }

    private function createOrGetCustomer($id)
    {
        $customer = Customer::where('id', $id)->first();

        if ($customer) {
            return $customer;
        }

        $customer     = new Customer;
        $customer->id = $id;
        $customer->save();

        return $customer->fresh();
    }

    private function createOrGetService($id, $data)
    {
        $service = Service::whereId($id)->first();

        if ($service) {
            return $service;
        }

        $service       = new Service;
        $service->id   = $id;
        $service->data = json_encode($data);
        $service->save();

        return $service->fresh();
    }

    private function createOrGetRequestLog($data)
    {

        $requestLog = RequestLogs::where('request', json_encode($data))->first();

        if ($requestLog) {
            return $requestLog;
        }

        $service          = new RequestLogs;
        $service->id      = Str::uuid()->toString();
        $service->request = $data;
        $service->save();

        return $service->fresh();
    }

    private function saveRequest($customer_id, $service_id, $request_log_id, $latencies)
    {
        $request = Request::where('request_log_id', $request_log_id)->first();

        if ($request) {
            return $request;
        }

        $request          = new Request();
        $request->id      = Str::uuid()->toString();
        $request->request = $latencies->request;
        $request->proxy   = $latencies->proxy;
        $request->kong    = $latencies->kong;

        $request->request_log_id = $request_log_id;
        $request->customer_id    = $customer_id;
        $request->service_id     = $service_id;

        $request->save();

        return $request->fresh();
    }

}
